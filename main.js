
import 'ol/ol.css';
import Map from 'ol/map';
import View from 'ol/view';
import TileLayer from 'ol/layer/tile';
import XYZSource from 'ol/source/xyz';
import proj from 'ol/proj';
import RasterSource from 'ol/source/raster';
import ImageLayer from 'ol/layer/image';

const key = 'pk.eyJ1IjoiZ2dhZmYxMSIsImEiOiJjamQ2Z3htcDU1YWpnMzRuc3AzMGpxa2pjIn0.do95ywcLWWIHz_eX3AYC6Q'; // eslint-disable-line

const map = new Map({ // eslint-disable-line no-new
  target: 'map-container',
  layers: [
    new TileLayer({
      source: new XYZSource({
        url: 'http://tile.stamen.com/terrain/{z}/{x}/{y}.jpg',
      }),
    }),
  ],
  view: new View({
    center: proj.fromLonLat([-71.06, 42.37]),
    zoom: 12,
  }),
});

const elevation = new XYZSource({
  url: 'https://api.mapbox.com/v4/mapbox.terrain-rgb/{z}/{x}/{y}.pngraw?access_token=' + key, // eslint-disable-line
  crossOrigin: 'anonymous',
});
function flood(pixels, data) {
  const pixel = pixels[0];
  if (pixel[3]) {
    // decode R, G, B values as elevation
    const height = -10000 + ((pixel[0] * 256 * 256 + pixel[1] * 256 + pixel[2]) * 0.1); // eslint-disable-line
    if (height <= data.level) {
      // sea blue
      pixel[0] = 145; // red
      pixel[1] = 175; // green
      pixel[2] = 186; // blue
      pixel[3] = 255; // alpha
    } else {
      // transparent
      pixel[3] = 0;
    }
  }
  return pixel;
}

const raster = new RasterSource({ // eslint-disable-line
  sources: [elevation],
  operation: flood,
});
const tile = new ImageLayer({ // eslint-disable-line no-new
  opacity: 0.8,
  source: raster,
});
map.addLayer(tile);
const control = document.getElementById('level'); // eslint-disable-line
const output = document.getElementById('output'); // eslint-disable-line
control.addEventListener('input', function () {  // eslint-disable-line
  output.innerText = control.value;
  raster.changed();
});
output.innerText = control.value;
raster.on('beforeoperations', function (event) {  // eslint-disable-line
  event.data.level = control.value;  // eslint-disable-line
});
