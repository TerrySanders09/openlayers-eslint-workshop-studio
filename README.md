Follow these instructions: [https://openlayers.org/workshop/en/](https://openlayers.org/workshop/en/)
- With one caveat, you will need to make sure your JavaScript complies with the eslint rules.
- The rules will be automatically checked everytime you save your code (and `npm start` is running in your terminal)